﻿// Specification https://github.com/Myle-Repos/node-services/wiki/Myle-Web-Services-Specification

var path = require("path"),
    moment = require('moment-timezone'),
    fs = require("fs"),
    mkdirp = require("mkdirp"),
    util = require("util"),
    uploadsDir = "uploads",
    dataFilePath = "uploads/data.json";


function moveFile(sourceFilePath, targetFilePath, done, fail) {

    var targetDir = path.dirname(targetFilePath);

    try {
        mkdirp.sync(targetDir, 0755);
    } catch (e) {
        fail(e);
        return;
    }

    // TODO: switch to approach that writes files directly into target dir from request
    // TODO: without going to /tmp one
    var input = fs.createReadStream(sourceFilePath);
    var output = fs.createWriteStream(targetFilePath);

    input.on("end", function() {
        fs.unlinkSync(sourceFilePath);
        done();
    }).on("error", function(err) {
        fail(err);
    });

    output.on("error", function(err) {
        fail(err);
    });

    input.pipe(output);
}


function insertDataRecords(accountId, newRecords, fail, isImport) {

    try {
        var dataFile = path.join(uploadsDir, accountId, "data.json");
        var records = fs.existsSync(dataFile)
            ? JSON.parse(fs.readFileSync(dataFile, { encoding: "utf8" }))
            : [];

        for (var i = 0, l = newRecords.length; i < l; i += 1) {

            var data = newRecords[i];

            var filtered = records.filter(function(i) { return timesAreEqual(i.audioTime, data.audioTime); });
            if (!isImport || filtered.length === 0) {
                records.push(data);
            }             
        }

        records.sort(function(a, b) {
            return a.audioTime.localeCompare(b.audioTime);
        });

        fs.writeFileSync(dataFile, JSON.stringify(records, null, "    "));

        return records.length - 1;
    } catch (e) {
        fail(e);
    }
}


function timesAreEqual(a, b) {
    console.log(a, b);
    for (var i = 0, l = Math.min(a.length, Math.min(b.length, 19)); i < l; i += 1) {
        if ((i >= 0 && i <= 9) || (i >= 11 && i <= 19)) {
            if (a[i] !== b[i]) {
                console.log("=", false);
                return false;
            }
        }
    }
    console.log("=", true);
    return true;
}


function assertError(condition, message, res) {
    if (!condition) {
        console.error(message);
        res.serverError();
    }
}


function assertRequest(req, message, res) {

    assertError(req.files && req.files.file, "No file uploaded", res);

    assertError(req.body, "No body", res);
    assertError(req.body.deviceId, "No deviceId", res);
    assertError(req.body.audioTime, "No audioTime", res);

    var audioTime = moment(req.body.audioTime, "YYYY-MM-DDTHH:mm:ss.SSSZ");
    assertError(audioTime.isValid(), "audioTime is invalid", res);

}


function getAccountId(deviceId) {
    
    // TODO: resolve accoutn ID from database

    return deviceId;
}


function readRecords(accountId, fail) {

    try {
        var dataFile = path.join(uploadsDir, accountId, "data.json");

        return fs.existsSync(dataFile)
            ? JSON.parse(fs.readFileSync(dataFile, { encoding: "utf8" }))
            : [];
    } catch (e) {
        fail(e);
    }

}


function importMapper(old) {
    var date = moment(old.date, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DDTHH:mm:ss.SSS") + "Z";
    return {
        audioTime: date,
        recordTime: date,
        location: {
            longitude: old.lng,
            latitude: old.lat
        },
        transcripts: [old.text],
        language: old.lang
    };
}


function getRecordsToImport(path, done, fail) {

    try {
        var records = fs.existsSync(path)
            ? JSON.parse(fs.readFileSync(path, { encoding: "utf8" }))
            : [];

        fs.unlinkSync(path);
        done(records);
    } catch (err) {
        fail(err);
    }
}


function toRecordView(record) {

    return {
        raw: record,
        text: (record.transcripts && record.transcripts.length)
            ? record.transcripts[0]
            : "[No message]",
        date: moment.utc(record.audioTime).tz("America/Edmonton").format("l LT"),
        mapUrl: record.location
            ? "https://maps.google.com/maps?q=" + record.location.latitude + "," + record.location.longitude
            : ""
    };

}



module.exports = {

    create: function (req, res) {

        assertRequest(req, res);

        var accountId = getAccountId(req.body.deviceId);
        var audioTime = moment(req.body.audioTime, "YYYY-MM-DDTHH:mm:ss.SSSZ");
        var creationDate = audioTime.format("YYYY-MM-DD");
        var creationTime = audioTime.format("HH:mm:ss.SSS");
        var targetFilePath = path.join(uploadsDir, accountId, creationDate, creationTime + path.extname(req.files.file.originalFilename));

        var errorHandler = function(err) {
            console.log(err);
            res.serverError();
        };

        moveFile(req.files.file.path, targetFilePath, function() {
            res.send({ recordId: insertDataRecords(accountId, [req.body], errorHandler) });
        }, errorHandler);

    },


    find: function(req, res) {

        var accountId = req.param('id');

        var errorHandler = function(err) {
            console.log(err);
            res.serverError();
        };

        var records = readRecords(accountId, errorHandler);
        console.log(require("util").inspect(records));
    
        res.view({
            title: "Account " + accountId,
            records: records.map(toRecordView)
        });

    },


    import: function(req, res) {
        
        var accountId = getAccountId(req.body.deviceId);
        var errorHandler = function(err) {
            console.log(err);
            res.serverError();
        };

        getRecordsToImport(req.files.file.path, function(records) {
            res.send({ count: insertDataRecords(accountId, records.map(importMapper), errorHandler, true) });
        }, errorHandler);

    }

};