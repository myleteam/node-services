/**
 * FileController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

var fs = require( 'fs' ),
    mkdirp = require( 'mkdirp' ),
    moment = require( "moment" ),
    path = require( "path" );

module.exports = {

    upload: function ( req, res ) {

        var now = moment.utc(),
            targetDir = path.join( "uploads", now.format( "YYYY-MM-DD" ) );
        //targetFile = path.join(targetDir, );
        try {
            mkdirp.sync( dirPath, 0755 );
        } catch ( e ) {
            console.log( e );
        }

        //var s = moment.utc().;
        //console.log(s);
        //res.send(s);
        //return;

        // TODO: switch to approach that writes files directly into target dir
        // TODO: without going to /tmp one
        var is = fs.createReadStream( req.files.file.path );
        var os = fs.createWriteStream( dirPath + "/" + req.files.file.originalFilename );

        is.pipe( os );
        is.on( 'end', function () {
            fs.unlinkSync( req.files.file.path );
            res.send( "done!" );
        });

        /*
        var file = dirPath + "/uploaded";
        
        if (req.readable) {
            // if request is readable: pipe it's stream to the output file
            var writeStream = fs.createWriteStream(file);
            writeStream.on('close', function () {
                console.log('All done!');
                res.send("done!");
            });
            req.pipe(writeStream);
        } else {
            console.log(req.body);
            // otherwise file content is in request body
            fs.writeFile(file, req.body, function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log('All done!');
                }
            });
            res.send("done!");
        }*/
    },


    /**
     * Overrides for the settings in `config/controllers.js`
     * (specific to FileController)
     */
    _config: {}


};
